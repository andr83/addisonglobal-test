# Addison Global Backend technical test task

## Introduction

The exercise implemented on Scala 2.12 with usage only Akka, Akka HTTP, Lightbend Config and ScalaTest libraries.
The `Cats` is in dependencies only as an example of alternative implementation of the first task.

1. The first task implementation in `TokenService.scala`  
2. The second task implementation in `SimpleAsyncTokenServiceImpl.scala`
3. The third one in `RestApi.scala`

## REST API

### Authentication. 

Request user token.

**URL**:   `/token`

**Method**: `POST`

**Data Parameters**:

  * username String 
  * password String
  
#### Success Response

**Code**: 200

**Content**: Token

**Example**:

```bash
curl -d "username=home&password=HOME" /token
home_2018-05-20T11:50:00Z
```

#### Error Response

1. If authentication failed:
  
    **Code**: 401

    **Content**: Error message
    
2. If user blocked(username starts from `A`):
  
    **Code**: 403

    **Content**: Error message    


## Build REST API service
### Docker

```bash
sbt docker:publishLocal
```

It will build local docker image `addisonglobal-test:0.1`

### Linux

I did not test linux native packages but they should work.

#### Debian
```bash
sbt debian:packageBin
```

#### RPM based distro
```bash
sbt rpm:packageBin
```

## Run REST API service in docker 
```bash
docker run -it --rm -p "8080:8080" addisonglobal-test:0.1
```

Test:
```bash
curl -d "username=house&password=HOUSE" localhost:8080/token
```

## REST API service configuration

By default service bind to `0.0.0.0` interface by `8080` port. It can be changed via environment variables:

* HTTP_HOST
* HTTP_PORT 