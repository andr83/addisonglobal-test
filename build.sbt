name := "addisonglobal-test"

version := "0.1"

scalaVersion := "2.12.6"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.2",
  "org.typelevel" %% "cats-core" % "1.1.0",
  "com.typesafe.akka" %% "akka-actor" % "2.5.12",
  "com.typesafe.akka" %% "akka-stream" % "2.5.12",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.12" % Test,
  "com.typesafe.akka" %% "akka-http" % "10.1.1",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test
)

dockerBaseImage := "openjdk:8-jre-alpine"
dockerExposedPorts := Seq(8080)

enablePlugins(AshScriptPlugin, JavaAppPackaging, DockerPlugin)