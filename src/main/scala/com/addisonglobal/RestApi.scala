package com.addisonglobal

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.{ExceptionHandler, HttpApp, Route}
import com.addisonglobal.error.{BlockedUser, InvalidCredentials}
import com.addisonglobal.model.Credentials

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Andrei Tupitcyn
  */
object RestApi extends HttpApp {
  implicit val system = ActorSystem("authTokenService")

  val authActor = system.actorOf(AuthActor.props(), "authActor")
  val tokenActor = system.actorOf(UserTokenActor.props(), "tokenActor")

  val tokenService = new SimpleAsyncTokenServiceImpl(authActor, tokenActor)

  private def errorHandler = ExceptionHandler {
    case e: InvalidCredentials =>
      complete(HttpResponse(Unauthorized, entity = e.getMessage))
    case e: BlockedUser =>
      complete(HttpResponse(Forbidden, entity = e.getMessage))
  }

  override def routes: Route =
    handleExceptions(errorHandler) {
      path("token") {
        post {
          formFields('username, 'password) { (username, password) =>
            complete {
              tokenService
                .requestToken(Credentials(username, password))
                .map(_.token)
            }
          }
        }
      }
    }

  def run(host: String, port: Int): Unit = {
    startServer(host, port, system)
    system.terminate()
  }
}
