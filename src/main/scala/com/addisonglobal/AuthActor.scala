package com.addisonglobal

import akka.actor.{Actor, ActorLogging, Props, Status}
import com.addisonglobal.model.Credentials

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Andrei Tupitcyn
  */
class AuthActor extends Actor with ActorLogging {
  import AuthActor._

  override def receive: Receive = {
    case Authenticate(credentials) =>
      val res = AuthService.authenticate(credentials)

      val delayMs = Random.nextInt(5000)
      log.info(s"Will delay authentication for $delayMs milliseconds")

      val message = res fold (Status.Failure, identity)
      context.system.scheduler.scheduleOnce(delayMs.milliseconds, sender(), message)
    case other =>
      log.error(s"Unsupported message $other in actor AuthActor")
  }
}

object AuthActor {
  case class Authenticate(credentials: Credentials)

  def props(): Props = Props(new AuthActor)
}
