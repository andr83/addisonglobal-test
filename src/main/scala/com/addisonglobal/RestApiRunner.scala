package com.addisonglobal

import com.typesafe.config.ConfigFactory

/**
  * @author Andrei Tupitcyn
  */
object RestApiRunner extends App {
  val conf = ConfigFactory.load()
  RestApi.run(conf.getString("http.host"), conf.getInt("http.port"))
}
