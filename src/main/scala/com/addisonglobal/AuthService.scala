package com.addisonglobal

import com.addisonglobal.error.InvalidCredentials
import com.addisonglobal.model.{Credentials, User}

/**
  * @author Andrei Tupitcyn
  */
object AuthService {
  def authenticate(
      credentials: Credentials): Either[InvalidCredentials, User] = {
    if (credentials.username.toUpperCase() == credentials.password) {
      Right(User(credentials.username))
    } else {
      Left(InvalidCredentials(credentials))
    }
  }
}
