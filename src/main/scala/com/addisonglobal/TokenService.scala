package com.addisonglobal

import cats.{Id, Monad}
import cats.implicits._

import com.addisonglobal.model.{Credentials, User, UserToken}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import scala.language.higherKinds

/**
  * @author Andrei Tupitcyn
  */
trait SyncTokenService {
  protected def authenticate(credentials: Credentials): User
  protected def issueToken(user: User): UserToken

  def requestToken(credentials: Credentials): UserToken = {
    val user = authenticate(credentials)
    issueToken(user)
  }
}

trait AsyncTokenService {
  protected def authenticate(credentials: Credentials): Future[User]
  protected def issueToken(user: User): Future[UserToken]

  def requestToken(credentials: Credentials): Future[UserToken] = {
    for {
      user <- authenticate(credentials)
      token <- issueToken(user)
    } yield token
  }
}

/** Also there is a more generic option to represent TokenService.
  * The problem with AsyncTokenService is it depends on scala Future which one
  * is also not the best for all cases. For example Monix task is more
  * lightweight or it can be cat's IO, FS2 or something else.
  *
  * @tparam F
  */
abstract class TokenService[F[_] : Monad] {
  protected def authenticate(credentials: Credentials): F[User]
  protected def issueToken(user: User): F[UserToken]

  def requestToken(credentials: Credentials): F[UserToken] = {
    for {
      user <- authenticate(credentials)
      token <- issueToken(user)
    } yield token
  }
}

// Sync implementation using cats' identity monad
class SyncTokenServiceImpl extends TokenService[Id] {
  override protected def authenticate(credentials: Credentials): Id[User] = ???

  override protected def issueToken(user: User): Id[UserToken] = ???
}

// Async implementation using scala Future
class AsyncTokenServiceImpl extends TokenService[Future] {
  override protected def authenticate(credentials: Credentials): Future[User] =
    ???

  override protected def issueToken(user: User): Future[UserToken] = ???
}
