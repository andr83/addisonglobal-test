package com.addisonglobal

import com.addisonglobal.model.{Credentials, User}

/**
  * @author Andrei Tupitcyn
  */
package object error {
  sealed abstract class AppError(message: String) extends Exception(message)

  case class InvalidCredentials(credentials: Credentials)
      extends AppError(s"Invalid credentials for user ${credentials.username}")

  case class BlockedUser(user: User)
      extends AppError(s"User ${user.userId} is blocked")
}
