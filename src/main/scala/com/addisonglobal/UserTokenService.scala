package com.addisonglobal

import java.time.{ZoneOffset, ZonedDateTime}
import java.time.format.DateTimeFormatter

import com.addisonglobal.error.BlockedUser
import com.addisonglobal.model.{User, UserToken}

/**
  * @author Andrei Tupitcyn
  */
object UserTokenService {
  private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")

  def issueToken(user: User): Either[BlockedUser, UserToken] = {
    if (user.userId.startsWith("A")) {
      Left(BlockedUser(user))
    } else {
      val now = ZonedDateTime.now(ZoneOffset.UTC)
      Right(UserToken(s"${user.userId}_${formatter.format(now)}"))
    }
  }
}
