package com.addisonglobal

import com.addisonglobal.model.{Credentials, UserToken}

import scala.concurrent.Future

/**
  * @author Andrei Tupitcyn
  */
trait SimpleAsyncTokenService {
  def requestToken(credentials: Credentials): Future[UserToken]
}
