package com.addisonglobal

/**
  * @author Andrei Tupitcyn
  */
package object model {
  case class Credentials(username: String, password: String)
  case class User(userId: String)
  case class UserToken(token: String)
}
