package com.addisonglobal
import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import com.addisonglobal.model.{User, UserToken}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Andrei Tupitcyn
  */
class SimpleAsyncTokenServiceImpl(authActor: ActorRef, tokenActor: ActorRef)
    extends SimpleAsyncTokenService {
  override def requestToken(
      credentials: model.Credentials): Future[model.UserToken] = {
    implicit val timeout: Timeout = Timeout(5.seconds)

    for {
      user <- (authActor ? AuthActor.Authenticate(credentials)).mapTo[User]
      token <- (tokenActor ? UserTokenActor.IssueToken(user)).mapTo[UserToken]
    } yield token
  }
}
