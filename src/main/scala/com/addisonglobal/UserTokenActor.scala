package com.addisonglobal

import akka.actor.{Actor, ActorLogging, Props, Status}
import com.addisonglobal.model.User

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * @author Andrei Tupitcyn
  */
class UserTokenActor extends Actor with ActorLogging {
  import UserTokenActor._

  override def receive: Receive = {
    case IssueToken(user) =>
      val res = UserTokenService.issueToken(user)

      val delayMs = Random.nextInt(5000)
      log.info(s"Will delay token issue for $delayMs milliseconds")

      val message = res.fold(Status.Failure, identity)
      context.system.scheduler.scheduleOnce(delayMs.milliseconds, sender(), message)
    case other =>
      log.error(s"Unsupported message $other in actor UserTokenActor")
  }
}

object UserTokenActor {
  case class IssueToken(user: User)

  def props(): Props = Props(new UserTokenActor)
}
