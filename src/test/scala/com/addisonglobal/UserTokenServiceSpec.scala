package com.addisonglobal

import java.time.{Instant, ZoneOffset, ZonedDateTime}
import java.time.format.DateTimeFormatter

import com.addisonglobal.error.BlockedUser
import com.addisonglobal.model.{User, UserToken}
import org.scalatest.{FlatSpec, Inside, Matchers}

/**
  * @author Andrei Tupitcyn
  */
class UserTokenServiceSpec extends FlatSpec with Matchers with Inside {
  private val formatter = DateTimeFormatter
    .ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
    .withZone(ZoneOffset.UTC)

  type NotOk = Left[BlockedUser, UserToken]

  def validateToken(userId: String): Unit = {
    val user = User(userId)
    val tokenResult = UserTokenService.issueToken(user)
    inside(tokenResult) {
      case Right(UserToken(token)) =>
        val parts = token.split('_')
        parts should have size 2
        parts(0) shouldBe userId
        val time = ZonedDateTime.parse(parts(1), formatter)
        val diff = Instant.now().getEpochSecond - time.toInstant.getEpochSecond
        diff should be <= 1L
    }
  }

  "The UserTokenService" should "fail if userId starts from `A`" in {
    UserTokenService.issueToken(User("Addisson")) shouldBe a[NotOk]
  }

  it should "return valid token in all other cases" in {
    validateToken("addisson")
    validateToken("hello")
    validateToken("Hello")
  }
}
