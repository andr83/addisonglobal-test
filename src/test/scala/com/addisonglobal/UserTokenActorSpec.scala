package com.addisonglobal

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.addisonglobal.model.{User, UserToken}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.concurrent.duration._

/**
  * @author Andrei Tupitcyn
  */
class UserTokenActorSpec
    extends TestKit(ActorSystem("AuthActorSpec"))
    with ImplicitSender
    with FlatSpecLike
    with Matchers
    with BeforeAndAfterAll {
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val tokenActor: ActorRef = system.actorOf(UserTokenActor.props())

  "An UserToken actor" should "respond in 5 seconds" in {
    tokenActor ! UserTokenActor.IssueToken(User("hello"))
    expectMsgType[UserToken](5.seconds)
  }

}
