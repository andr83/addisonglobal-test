package com.addisonglobal

import akka.http.scaladsl.model.{FormData, StatusCodes}
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import org.scalatest.{FlatSpec, Matchers}
import scala.concurrent.duration._

/**
  * @author Andrei Tupitcyn
  */
class RestApiSpec extends FlatSpec with Matchers with ScalatestRouteTest {
  implicit val timeout: RouteTestTimeout = RouteTestTimeout(10.seconds)

  "A rest api" should "return token for valid POST request" in {
    Post("/token", FormData("username" -> "hello", "password" -> "HELLO")) ~> RestApi.routes ~> check {
      status shouldEqual StatusCodes.OK
      responseAs[String] should startWith("hello_")
    }
  }

  it should "return 401(Unauthorized) code on wrong credentials" in {
    Post("/token", FormData("username" -> "hello", "password" -> "hello")) ~> RestApi.routes ~> check {
      status shouldEqual StatusCodes.Unauthorized
    }
  }

  it should "return 403(Forbidden) code if user blocked" in {
    Post("/token", FormData("username" -> "Amber", "password" -> "AMBER")) ~> RestApi.routes ~> check {
      status shouldEqual StatusCodes.Forbidden
    }
  }
}
