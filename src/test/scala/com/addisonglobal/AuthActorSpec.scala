package com.addisonglobal

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.addisonglobal.model.{Credentials, User}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.concurrent.duration._

/**
  * @author Andrei Tupitcyn
  */
class AuthActorSpec
    extends TestKit(ActorSystem("AuthActorSpec"))
    with ImplicitSender
    with FlatSpecLike
    with Matchers
    with BeforeAndAfterAll {
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val authActor: ActorRef = system.actorOf(AuthActor.props())

  "An Auth actor" should "respond in 5 seconds" in {
    authActor ! AuthActor.Authenticate(Credentials("hello", "HELLO"))
    expectMsg(5.seconds, User("hello"))
  }

  it should "not respond for any other message" in {
    authActor ! "Hello"
    expectNoMessage(1.second)
  }
}
