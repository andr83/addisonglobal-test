package com.addisonglobal

import com.addisonglobal.error.AppError
import com.addisonglobal.model.{Credentials, User}
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Andrei Tupitcyn
  */
class AuthServiceSpec extends FlatSpec with Matchers {
  type NotOk = Left[AppError, User]

  "The AuthService" should "return success if the username in uppercase matches the password" in {
    AuthService.authenticate(Credentials("house", "HOUSE")) shouldBe Right(User("house"))
    AuthService.authenticate(Credentials("House", "HOUSE")) shouldBe Right(User("House"))
    AuthService.authenticate(Credentials("HOUSE", "HOUSE")) shouldBe Right(User("HOUSE"))
  }

  it should "return error in other cases" in {
    AuthService.authenticate(Credentials("house", "house")) shouldBe a[NotOk]
    AuthService.authenticate(Credentials("House", "House")) shouldBe a[NotOk]
    AuthService.authenticate(Credentials("HOUSE", "house")) shouldBe a[NotOk]
  }
}
